const express = require('express');
const router = express.Router();
const path = require('path');

//Middleware para mostrar datos del request
router.use (function (req,res,next) {
  console.log('/' + req.method);
  next();
});

//Analiza la ruta y el protocolo y reacciona en consecuencia
router.get('/',function(req,res){
  res.sendFile(path.resolve('views/index.html'));
});

// Ruta post formulario edad
router.post('/mayor',function(req,res){
  res.sendFile(path.resolve('views/process.html')), {myData: 'hi'};
  console.log(req.body.age);
});

module.exports = router;