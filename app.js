var express = require('express');
var app = express();

// require routes and views
const index = require('./routes/index');
const path = __dirname + '/views/';

// link the template engine (plain html)
app.set('view engine', 'html');
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path));

app.use('/', index);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
